﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Enrollment No")]
        [RegularExpression("[0-9]{12}", ErrorMessage = "Enrollment No Is Invalid!!")]
        [Required]
        public string EnrollmentNo { get; set; }

        [DisplayName("Student Name")]
        [MinLength(3, ErrorMessage = "Minimum Length Of Name Should Be 3!!")]
        [Required]

        public string Name { get; set; }

        [DisplayName("Password")]
        [MinLength(8, ErrorMessage = "Minimum Length Of Password Should Be 8!!")]
        [Required]
        public string Password { get; set; }

        public bool IsApproved { get; set; }
    }
}