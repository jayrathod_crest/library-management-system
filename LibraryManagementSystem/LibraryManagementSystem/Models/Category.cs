﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Category")]
        public string CategoryName { get; set; }
    }
}