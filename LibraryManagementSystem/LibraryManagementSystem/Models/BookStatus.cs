﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Models
{
    public class BookStatus
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Student Id")]
        [Required]
        public virtual Student StudentId { get; set; }

        [DisplayName("Book Id")]
        [Required]
        public virtual Book BookId { get; set; }

        [DisplayName("Book Status")]
        [Required]
        public BookStatusOptions Status { get; set; }

        [DisplayName("Ordered Date")]
        [Required]
        public DateTime OrderedDate { get; set; }

        [DisplayName("Issued Date")]
        public DateTime IssuedDate { get; set; }

        [DisplayName("Returned Date")]
        public DateTime ReturnedDate { get; set; }
    }
}