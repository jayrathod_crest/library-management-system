﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Book Name")]
        [MinLength(3, ErrorMessage = "Minimum Length Of Book Name Should Be 3!!")]
        [Required]
        public string Name { get; set; }

        [DisplayName("Author Name")]
        [MinLength(3, ErrorMessage = "Minimum Length Of Author Name Should Be 3!!")]
        [Required]
        public string Author { get; set; }

        [DisplayName("Quantity Of Book")]
        [Required]
        public int Quantity { get; set; }

        [DisplayName("Category Of Book")]
        [Required]
        public virtual Category Category { get; set; }
    }
}