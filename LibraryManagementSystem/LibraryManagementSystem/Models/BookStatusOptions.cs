﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Models
{
    public enum BookStatusOptions
    {
        BookOrdered = 1,
        BookIssued = 2,
        BookIssueRejected = 3,
        BookAppliedForReturn = 4,
        BookReturned = 5
    }
}