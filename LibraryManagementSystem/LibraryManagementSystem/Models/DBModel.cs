﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Models
{
    public class DBModel: DbContext
    {
        public DBModel() : base("LibraryManagementSystem")
        {

        }

        public DbSet<Admin> AdminEntity { get; set; }
        public DbSet<Student> StudentEntity { get; set; }
        public DbSet<Category> CategoryEntity { get; set; }
        public DbSet<Book> BookEntity { get; set; }
        public DbSet<BookStatus> BookStatusEntity { get; set; }
    }
}