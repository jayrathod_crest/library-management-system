﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Helpers
{
    public class Paginator
    {
    }
    public class Page<T>
    {
        public int currentPage { get; set; }
        public bool HasPrev { get; set; }
        public bool HasNext { get; set; }
        public List<T> Items { get; set; }

    }

    public class Paginator<T>
    {
        private int itemsPerPage = 7, totalPages = 0;
        private List<T> items;

        public Paginator(List<T> items)
        {
            this.items = items;
            this.totalPages = (int)Math.Ceiling((decimal)items.Count / itemsPerPage);
        }

        public bool HasPrev(int currentPage)
        {
            return currentPage > 1;
        }

        public bool HasNext(int currentPage)
        {
            return currentPage < totalPages;
        }

        public Page<T> Page(int page)
        {
            page = Math.Max(1, page);
            page = Math.Min(page, totalPages);

            return new Page<T>() {
                currentPage = page,
                HasPrev = HasPrev(page),
                HasNext = HasNext(page),
                Items = items.Skip((page - 1) * itemsPerPage).Take(itemsPerPage).ToList()
            };
        }
    }
}