﻿using LibraryManagementSystem.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryManagementSystem.Controllers
{
    public class HomeController : Controller
    {
        private DBModel _db = new DBModel();

        public ActionResult Index()
        {
            return View();
        }
    }
}