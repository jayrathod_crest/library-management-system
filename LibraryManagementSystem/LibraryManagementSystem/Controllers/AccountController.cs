﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryManagementSystem.Models;
using System.Web.Security;
using System.Web.Mvc;
using System.Data.Entity.Validation;

namespace LibraryManagementSystem.Controllers
{
    public class AccountController : Controller
    {

        DBModel Entity = new DBModel();

        public ActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Signup()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel Credentials)
        {
            bool isExist;
            
            Admin admin = null;
            
            Student std = null;
            
            if (Credentials.UserName == "Admin" || Credentials.UserName == "admin")
            {
                isExist = Entity.AdminEntity.Any(x => x.Username == Credentials.UserName && x.Password == Credentials.Password);

                admin = Entity.AdminEntity.FirstOrDefault(x => x.Username == Credentials.UserName && x.Password == Credentials.Password);
            }
            else
            {
                isExist = Entity.StudentEntity.Any(x => x.EnrollmentNo == Credentials.UserName && x.Password == Credentials.Password);

                std = Entity.StudentEntity.FirstOrDefault((x => x.EnrollmentNo == Credentials.UserName && x.Password == Credentials.Password));

                
            }

            if (isExist)
            {
                if(admin != null)
                {
                    Session["admin"] = admin.Username;

                    return RedirectToAction("requestedBook","Admin");
                }
                else
                {
                    if (std.IsApproved)
                    {
                        Session["student"] = std.Id;
                        
                        return RedirectToAction("ShowBook", "Student");
                    }

                    ModelState.AddModelError("", "User is not approved by Admin!!");

                    return View();

                }
            }

            ModelState.AddModelError("","Username or Password is wrong!!");

            return View();
        }
         
        [HttpPost]
        public ActionResult Signup(Student StudentInfo)
        {
            try
            {
                Student student = Entity.StudentEntity.FirstOrDefault<Student>(x => x.EnrollmentNo == StudentInfo.EnrollmentNo);
                if(student != null)
                {
                    ModelState.AddModelError("", "Enrollment No is already registered!!");

                    return View();
                }

                StudentInfo.IsApproved = false;

                Entity.StudentEntity.Add(StudentInfo);

                Entity.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        Console.WriteLine("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                    }
                }
            }

            return RedirectToAction("Login");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            if(Session["student"] != null)
            {
                Session["student"] = null;
            }
            else
            {
                Session["admin"] = null;
            }
            return RedirectToAction("Login");
        }
    }
}