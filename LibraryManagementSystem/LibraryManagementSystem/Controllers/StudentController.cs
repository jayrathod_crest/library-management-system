﻿using LibraryManagementSystem.Helpers;
using LibraryManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace LibraryManagementSystem.Controllers
{
    public class StudentController : Controller
    {
        private DBModel db = new DBModel();

        [HttpGet]
        public ActionResult ShowBook(int page = 1)
        {
            if(Session["student"] == null)
            {
                return RedirectToAction("login", "account");
            }
            List<Book> ls = db.BookEntity.ToList();
            Paginator<Book> paginator = new Paginator<Book>(ls);

            return View(paginator.Page(page));
        }

        [HttpPost]
        public ActionResult ShowBook(string search, int option, int page = 1)
        {
            TempData["search"] = search;
            TempData["option"] = option;

            if (option == 2)
            {
                Paginator<Book> paginator = new Paginator<Book>(db.BookEntity.Where(x => x.Author.Contains(search)).ToList());
                return View(paginator.Page(page));
            }
            else if (option == 3)
            {
                Paginator<Book> paginator = new Paginator<Book>(db.BookEntity.Where(x => x.Category.CategoryName.Contains(search)).ToList());
                return View(paginator.Page(page));
            }
            else
            {
                Paginator<Book> paginator = new Paginator<Book>(db.BookEntity.Where(x => x.Name.Contains(search)).ToList());
                return View(paginator.Page(page));
            }
        }

        [HttpGet]
        public ActionResult OrderedBook(int? id)
        {
            if (Session["student"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Book book = db.BookEntity.Find(id);
            Student student = db.StudentEntity.Find(Convert.ToInt32(Session["student"].ToString()));
            BookStatus bookStatus = new BookStatus();
            bookStatus.BookId = book;
            bookStatus.StudentId = student;
            bookStatus.Status = BookStatusOptions.BookOrdered;
            bookStatus.IssuedDate = DateTime.Now;
            bookStatus.OrderedDate = DateTime.Now;
            bookStatus.ReturnedDate = DateTime.Now.AddDays(7);
            db.BookStatusEntity.Add(bookStatus);
            db.SaveChanges();

            return RedirectToAction("ShowOrderedBook");
        }

        [HttpGet]
        public ActionResult ShowOrderedBook(int page = 1)
        {
            if (Session["student"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Student student = db.StudentEntity.Find(Convert.ToInt32(Session["student"].ToString()));
            List<BookStatus> ls = db.BookStatusEntity.ToList();
            List<BookStatus> OrderedBook = new List<BookStatus>();
            foreach (BookStatus lsItem in ls)
            {
                if (lsItem.StudentId == student && lsItem.Status == BookStatusOptions.BookOrdered)
                {
                    OrderedBook.Add(lsItem);
                }
            }


            Paginator<BookStatus> paginator = new Paginator<BookStatus>(OrderedBook);

            return View(paginator.Page(page));
        }

        [HttpGet]
        public ActionResult ShowIssuedBook(int page = 1)
        {
            if (Session["student"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Student student = db.StudentEntity.Find(Convert.ToInt32(Session["student"].ToString()));
            List<BookStatus> ls = db.BookStatusEntity.ToList();
            List<BookStatus> issuedBook = new List<BookStatus>();


            foreach (BookStatus lsItem in ls)
            {
                if (lsItem.StudentId == student && lsItem.Status == BookStatusOptions.BookIssued)
                {
                    issuedBook.Add(lsItem);
                }
            }

            Paginator<BookStatus> paginator = new Paginator<BookStatus>(issuedBook);

            return View(paginator.Page(page));
        }

        [HttpGet]
        public ActionResult ReturnBook(int? id)
        {
            if (Session["student"] == null)
            {
                return RedirectToAction("login", "account");
            }

            BookStatus bookStatus=db.BookStatusEntity.Find(id);
            Book book = bookStatus.BookId;
            Student student =bookStatus.StudentId;

            bookStatus.Status = BookStatusOptions.BookReturned;
            bookStatus.ReturnedDate = DateTime.Now;

            db.SaveChanges();

            return RedirectToAction("ShowReturnBook");

        }
        [HttpGet]
        public ActionResult ShowReturnBook(int page = 1)
        {
            if (Session["student"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Student student = db.StudentEntity.Find(Convert.ToInt32(Session["student"].ToString()));
            List<BookStatus> ls = db.BookStatusEntity.ToList();
            List<BookStatus> returnBook = new List<BookStatus>();
            foreach (BookStatus lsItem in ls)
            {
                if (lsItem.StudentId == student && lsItem.Status == BookStatusOptions.BookReturned)
                {
                    returnBook.Add(lsItem);
                }
            }

            Paginator<BookStatus> paginator = new Paginator<BookStatus>(returnBook);

            return View(paginator.Page(page));
        }

        public ActionResult ShowRejectedBook(int page = 1)
        {
            if (Session["student"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Student student = db.StudentEntity.Find(Convert.ToInt32(Session["student"].ToString()));
            List<BookStatus> ls = db.BookStatusEntity.ToList();
            List<BookStatus> rejectedBook = new List<BookStatus>();
            foreach (BookStatus lsItem in ls)
            {
                if (lsItem.StudentId == student && lsItem.Status == BookStatusOptions.BookIssueRejected)
                {
                    rejectedBook.Add(lsItem);
                }
            }

            Paginator<BookStatus> paginator = new Paginator<BookStatus>(rejectedBook);

            return View(paginator.Page(page));
        }

    }
}