﻿using LibraryManagementSystem.Helpers;
using LibraryManagementSystem.Models;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace LibraryManagementSystem.Controllers
{
    public class BookController : Controller
    {
        private DBModel _db = new DBModel();

        public ActionResult Index(int page = 1)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Paginator<Book> paginator = new Paginator<Book>(_db.BookEntity.ToList());

            return View(paginator.Page(page));
        }

        [HttpPost]
        public ActionResult Index(string search, int option, int page = 1)
        {

            TempData["search"] = search;
            TempData["option"] = option;
            if (option == 2)
            {
                Paginator<Book> paginator = new Paginator<Book>(_db.BookEntity.Where(x => x.Author.Contains(search)).ToList());
                return View(paginator.Page(page));
            }                
            else if (option == 3)
            {
                Paginator<Book> paginator = new Paginator<Book>(_db.BookEntity.Where(x => x.Category.CategoryName.Contains(search)).ToList());
                return View(paginator.Page(page));
            }
            else
            {
                Paginator<Book> paginator = new Paginator<Book>(_db.BookEntity.Where(x => x.Name.Contains(search)).ToList());
                return View(paginator.Page(page));
            }
        }


        public ActionResult Add()
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }

            TempData["category_options"] = GetSelectListItems();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(Book book, int category)
        {
            book.Category = _db.CategoryEntity.Find(category);
            _db.BookEntity.Add(book);
            _db.SaveChanges();
            return RedirectToAction("Index", "Book");
        }

        public ActionResult Edit(int? id)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TempData["category_options"] = GetSelectListItems();
            Book book = _db.BookEntity.Find(id);
            if(book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Book book, int category)
        {
            //book.Category = _db.CategoryEntity.Find(category);
            //_db.Entry(book).State = EntityState.Modified;
            Book original = _db.BookEntity.Find(book.Id);
            original.Name = book.Name;
            original.Author = book.Author;
            original.Quantity = book.Quantity;
            original.Category = _db.CategoryEntity.Find(category);
            _db.SaveChanges();
            return RedirectToAction("Index", "Book");
        }

        public ActionResult Delete(int? id)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = _db.BookEntity.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Book book)
        {
            _db.BookEntity.Remove(_db.BookEntity.Find(book.Id));
            _db.SaveChanges();
            return RedirectToAction("Index", "Book");
        }

        public List<SelectListItem> GetSelectListItems()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (Category item in _db.CategoryEntity)
            {
                list.Add(new SelectListItem() { Value = item.Id.ToString(), Text = item.CategoryName });
            }
            return list;
        }
    }
}