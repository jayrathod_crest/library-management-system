﻿using LibraryManagementSystem.Models;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;
using LibraryManagementSystem.Helpers;
using System.Globalization;
using System.Web.UI;

namespace LibraryManagementSystem.Controllers
{
    public class AdminController : Controller
    {
      
        private DBModel _context = new DBModel();

        

        [HttpGet]
        public ActionResult PendingAccount(int page = 1)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }

            var pendingStudentList = _context.StudentEntity.Where(Student => Student.IsApproved == false).OrderBy(x => x.Id).ToList();
            Paginator<Student> paginator = new Paginator<Student>(pendingStudentList);

            return View(paginator.Page(page));
        }

        [HttpGet]
        public ActionResult Approve(int id)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }

            var Student = _context.StudentEntity.FirstOrDefault(student => student.IsApproved == false && student.Id == id);

            if (Student == null)
                return HttpNotFound();

            Student.IsApproved = true;

            _context.SaveChanges();

            TempData["ApproveStatus"] = "Student is Approved!";

            return RedirectToAction("PendingAccount");

        }

        [HttpGet]
        public ActionResult RequestedBook(int page = 1, string bookName = null, string studentId = null)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }
            var books = _context.BookStatusEntity.Where(book => book.Status == BookStatusOptions.BookOrdered).ToList();

            if (!String.IsNullOrEmpty(bookName))
                books = books.Where(b => b.BookId.Name.StartsWith(bookName, true, CultureInfo.InvariantCulture)).ToList();

            if (!String.IsNullOrEmpty(studentId))
                books = books.Where(b => b.StudentId.Id.ToString() == studentId).ToList();

            Paginator<BookStatus> paginator = new Paginator<BookStatus>(books);

            return View(paginator.Page(page));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveBook(int id)
        {
            BookStatus book = _context.BookStatusEntity.FirstOrDefault(b => b.Id == id && b.Status == BookStatusOptions.BookOrdered);

            if (book is null)
                return HttpNotFound();

            if (book.BookId.Quantity > 0)
            {

                TempData["MessageStatus"] = "success";
                TempData["Message"] = $"Book Order {id} - &nbsp;<strong>{book.BookId.Name}</strong>&nbsp; issued to Student {book.StudentId.Id} successfully.";

                book.BookId.Quantity -= 1;
                book.BookId.Category = book.BookId.Category;
                book.BookId = book.BookId;
                book.StudentId = book.StudentId;
                book.IssuedDate = DateTime.Now;
                book.Status = BookStatusOptions.BookIssued;
                _context.SaveChanges();
            } 
            else
            {
                TempData["MessageStatus"] = "danger";
                TempData["Message"] = $"Sorry! all copies of that book are already issued.";

                book.BookId = book.BookId;
                book.StudentId = book.StudentId;
                book.Status = BookStatusOptions.BookIssueRejected;
                _context.SaveChanges();
            }

            return RedirectToAction("RequestedBook");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RejectBook(int id)
        {
            BookStatus book = _context.BookStatusEntity.FirstOrDefault(b => b.Id == id && b.Status == BookStatusOptions.BookOrdered);

            if (book is null)
                return HttpNotFound();

            TempData["MessageStatus"] = "danger";
            TempData["Message"] = $"Book order with ID {id} is rejected";

            book.BookId = book.BookId;
            book.StudentId = book.StudentId;
            book.Status = BookStatusOptions.BookIssueRejected;
            _context.SaveChanges();

            return RedirectToAction("RequestedBook");
        }

        [HttpGet]
        public ActionResult IssuedBook(int page = 1, string bookName = null, string studentId = null)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }

            var books = _context.BookStatusEntity.Where(b => b.Status == BookStatusOptions.BookIssued).ToList();

            if (!String.IsNullOrEmpty(bookName))
                books = books.Where(book => book.BookId.Name.StartsWith(bookName, true, CultureInfo.InvariantCulture)).ToList();

            if (!String.IsNullOrEmpty(studentId))
                books = books.Where(book => book.StudentId.EnrollmentNo.ToString() == studentId).ToList();

            Paginator<BookStatus> paginator = new Paginator<BookStatus>(books);

            return View(paginator.Page(page));
        }

        [HttpGet]
        public ActionResult ReturnBook(int id)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("login", "account");
            }

            var book = _context.BookStatusEntity.FirstOrDefault(b => b.Id == id && b.Status == BookStatusOptions.BookIssued);

            if (book is null)
                return HttpNotFound();

            DateTime today = DateTime.Now;

            if (book.ReturnedDate.CompareTo(today) < 0) {
                int extraDays = today.Subtract(book.ReturnedDate).Days;
                ViewBag.Fine = extraDays * 50;

                TempData["MessageStatus"] = "danger";
                TempData["Message"] = $"Fine Rs. {ViewBag.Fine} - As student is late by {extraDays} days";
            } 
            else
            {
                TempData["MessageStatus"] = "success";
                TempData["Message"] = $"Way to go! Book returned on time.";
            }

            return View(book);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveReturnBook(int id)
        {
            var book = _context.BookStatusEntity.FirstOrDefault(b => b.Id == id && b.Status == BookStatusOptions.BookIssued);

            if (book is null)
                return HttpNotFound();

            book.BookId.Quantity += 1;
            book.BookId.Category = book.BookId.Category;
            book.BookId = book.BookId;
            book.StudentId = book.StudentId;
            book.Status = BookStatusOptions.BookReturned;
            _context.SaveChanges();

            TempData["MessageStatus"] = "success";
            TempData["Message"] = $"Book - {book.BookId.Name} returned successfully";

            return RedirectToAction("IssuedBook");
        }

      
    }
}